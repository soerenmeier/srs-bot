﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using NLog;
using RurouniJones.OverlordBot.Administration;
using RurouniJones.OverlordBot.Diagnostics;

namespace RurouniJones.OverlordBot.Discord.Commands
{
    [Group("overlordbot")]
    internal class RadioControllerModule : ModuleBase<SocketCommandContext>
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly Dictionary<Guid, DisableLogEntry> CommandInitiators = new();

        [Command("status")]
        [Summary("Return the status of all controllers running on this bot")]
        internal async Task GetControllerStatusAsync()
        {
            var controllerStatuses = Collector.ControllerStatuses.ToList();

            if (controllerStatuses.Count == 0)
            {
                await ReplyAsync($"{Collector.BotName}\nNo controller found");
                return;
            }

            await ReplyWithStatusEmbed(controllerStatuses.Select(controllerStatus => new DiscordStatusEntry(controllerStatus, CommandInitiators.ContainsKey(controllerStatus.Id) ? CommandInitiators[controllerStatus.Id] : null )).ToList());
        }

        [Command("status")]
        [Summary("Return the status of all controllers running on this bot for the specified server")]
        internal async Task GetControllerStatusOnServerAsync(
            [Summary("Short name of the server you wish to check")]
            string serverShortName)
        {
            var controllerStatuses = Collector.ControllerStatuses.Where(x => string.Equals(x.ServerShortName, serverShortName, StringComparison.CurrentCultureIgnoreCase)).ToList();
            if(controllerStatuses.Count == 0)
            {
                await ReplyAsync($"No servers found with short name \"{serverShortName}\"");
            }
            else
            {

                if (controllerStatuses.Count == 0)
                {
                    await ReplyAsync($"{Collector.BotName}\nNo controller found");
                    return;
                }

                await ReplyWithStatusEmbed(controllerStatuses.Select(controllerStatus => new DiscordStatusEntry(controllerStatus, CommandInitiators.ContainsKey(controllerStatus.Id) ? CommandInitiators[controllerStatus.Id] : null )).ToList());
            }
        }

        [Command("enable")]
        [Summary("Enable the specified controller so that it starts responding to transmissions")]
        internal async Task EnableControllerOnServerAsync(
            [Summary("Short name of the server the controller is running on")]
            string serverShortName, 
            [Summary("Frequency the controller is using")]
            string frequency
            )
        {
            if (!Configuration.EnableDiscordCommands)
            {
                await ReplyAsync($"This command is disabled by the administrators");
                return;
            }
            if (Context.IsPrivate)
            {
                await ReplyAsync($"This command cannot be called in private messages");
                return;
            }

            var controller = Collector.ControllerStatuses.FirstOrDefault(x =>
                string.Equals(x.ServerShortName, serverShortName, StringComparison.CurrentCultureIgnoreCase) && x.Frequency.ToString(CultureInfo.InvariantCulture) == frequency);
            if (controller == null)
            {
                await ReplyAsync($"No controller listening on {serverShortName} {frequency}");
                return;
            }
            CommandQueue.Commands.Enqueue(new CommandQueue.EnableControllerCommand(controller.Id));
            await ReplyAsync($"Enabling {controller.ServerShortName} {controller.ControllerName} on {frequency}");
            _logger.Info($"Controller {controller.ServerShortName} {controller.ControllerName} ENABLED by {Context.User.Username} (Discord ID: {Context.User.Id}) on {Context.Channel.Name}");
        }

        [Command("disable")]
        [Summary("Disable the specified controller so that it stops responding to transmissions")]
        internal async Task DisableControllerOnServerAsync(
            [Summary("Short name of the server the controller is running on")]
            string serverShortName, 
            [Summary("Frequency the controller is using")]
            string frequency,
            [Remainder]
            [Summary("Reason for stopping the controller (e.g. \"Human ATC Online\"")]
            string reason 
        )
        {
            if (!Configuration.EnableDiscordCommands)
            {
                await ReplyAsync($"This command is disabled by the administrators");
                return;
            }

            if (Context.IsPrivate)
            {
                await ReplyAsync($"This command cannot be called in private messages");
                return;
            }

            var controller = Collector.ControllerStatuses.FirstOrDefault(x =>
                string.Equals(x.ServerShortName, serverShortName, StringComparison.CurrentCultureIgnoreCase) && x.Frequency.ToString(CultureInfo.InvariantCulture) == frequency);
            if (controller == null)
            {
                await ReplyAsync($"No controller listening on {serverShortName} {frequency}");
                return;
            }

            CommandQueue.Commands.Enqueue(new CommandQueue.DisableControllerCommand(controller.Id));
            await ReplyAsync($"Disabling {controller.ServerShortName} {controller.ControllerName} on {frequency}");
            _logger.Info($"Controller {controller.ServerShortName} {controller.ControllerName} DISABLED by {Context.User.Username} (Discord ID: {Context.User.Id}) on #{Context.Channel.Name}. Reason: {reason}");  
            
            CommandInitiators[controller.Id] = new DisableLogEntry(DateTime.Now, Context.User.Username, Context.User.Id, reason);
        }

        private async Task ReplyWithStatusEmbed(IEnumerable<DiscordStatusEntry> entries)
        {
            var embed = new EmbedBuilder()
                .WithColor(Color.DarkGreen)
                .WithCurrentTimestamp();

            foreach (var entry in entries)
            {
                var sb = new StringBuilder();
                if (!entry.ControllerStatus.IsConnected)
                {
                    sb.Append("_Not connected to SRS_");
                }
                else
                {
                    sb.Append("Connected to SRS. ");
                    sb.Append(entry.ControllerStatus.LastTransmissionAgo());
                }

                embed.AddField(
                    $"{entry.ControllerStatus.ServerShortName} {entry.ControllerStatus.ControllerName} on {entry.ControllerStatus.Frequency} {entry.ControllerStatus.Modulation}",
                    sb.ToString());

                if (!entry.ControllerStatus.IsConnected || entry.ControllerStatus.RespondsToTransmissions) continue;
                embed.AddField("Status", $"Disabled by {entry.LogEntry.Username}", true);
                embed.AddField("Reason", $"\"{entry.LogEntry.Reason}\" {entry.LogEntry.DisabledAgo()}", true);
            }

            try
            {
                var privateChannel = await Context.User.CreateDMChannelAsync();
                await privateChannel.SendMessageAsync(embed: embed.Build());
                await Context.Message.AddReactionAsync(new Emoji("✅"));
            }
            catch (Exception ex)
            {
                _logger.Warn(ex);
                await Context.Message.AddReactionAsync(new Emoji("❎"));
            }
        }

        private class DiscordStatusEntry
        {
            public ControllerStatus ControllerStatus { get; }
            public DisableLogEntry LogEntry { get; }

            public DiscordStatusEntry(ControllerStatus controllerStatus, DisableLogEntry logEntry = null)
            {
                ControllerStatus = controllerStatus;
                LogEntry = logEntry;
            }

        }

        private class DisableLogEntry
        {
            private DateTime Timestamp { get; }
            public string Username { get; }
            private ulong UserId { get; }
            public string Reason { get; }

            public DisableLogEntry(DateTime timeStamp, string username, ulong userId, string reason)
            {
                Timestamp = timeStamp;
                Username = username;
                UserId = userId;
                Reason = reason;
            }

            public string DisabledAgo()
            {
                var diff = DateTime.Now - Timestamp;

                // ReSharper disable once ConvertIfStatementToSwitchStatement
                if (diff.Days > 7)
                {
                    return null;
                }
                if (diff.Days > 0)
                {
                    return $"{diff.Days} days ago";
                }
                if (diff.Hours > 0)
                {
                    return $"{diff.Hours} hours ago";
                }
                if (diff.Minutes > 0)
                {
                    return $"{diff.Minutes} minutes ago";
                }
                // ReSharper disable once ConvertIfStatementToReturnStatement
                if (diff.Seconds > 0)
                {
                    return $"{diff.Seconds} seconds ago";
                }

                return null;
            }
        }
    }
}
