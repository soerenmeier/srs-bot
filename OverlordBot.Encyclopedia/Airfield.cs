﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;

namespace RurouniJones.OverlordBot.Encyclopedia
{
    public class Airfield
    { 
        private static readonly IDeserializer Deserializer = new DeserializerBuilder().Build();

        public static readonly HashSet<Airfield> Entries = PopulateAirfields();

        private static HashSet<Airfield> PopulateAirfields()
        {
            var airfields = new HashSet<Airfield>();

            foreach (var file in Directory.GetFiles("Data/Encyclopedia/Airfields", "*.yaml"))
            {
                airfields.Add(Deserializer.Deserialize<Airfield>(File.ReadAllText(file)));
            }
            return airfields;
        }

        /// <summary>
        /// Name of the Airfield.
        /// </summary>
        [YamlMember(Alias = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Decimal latitude (e.g. 41.12324)
        /// </summary>
        [YamlMember(Alias = "lat")]
        public double Latitude { get; set; }

        /// <summary>
        /// Decimal Longitude (e.g. 37.12324)
        /// </summary>
        [YamlMember(Alias = "lon")]
        public double Longitude { get; set; }

        /// <summary>
        /// Altitude in Meters
        /// </summary>
        [YamlMember(Alias = "alt")]
        public double Altitude { get; set; }

        /// <summary>
        /// A list of Parking spots
        /// 
        /// A Parking Spot includes anywhere where aircraft might spawn. So this needs to cover areas where a mission
        /// maker might spawn "OnGround" aircraft such as Harriers. For this reason the Parking Spots at Anapa include
        /// the Maintenance Area as Harriers are spawned there on GAW.
        /// </summary>
        [YamlMember(Alias = "parking_spots")]
        public HashSet<ParkingSpot> ParkingSpots { get; set; } = new();

        /// <summary>
        /// A list of Runways 
        /// </summary>
        [YamlMember(Alias = "runways")]
        public HashSet<Runway> Runways { get; set; } = new();

        /// <summary>
        /// A list of paths that link nodes that an aircraft can travel along.
        /// 
        /// A  with a specific source and target NavigationPoint.
        /// If it is possible to go in both directions between nodes then there need to be two navigation paths, one going each way.
        /// </summary>
        [YamlMember(Alias = "navigation_paths")]
        public List<NavigationPath> NavigationPaths { get; set; } = new();

        /// <summary>
        /// A list of Taxi Junctions
        /// 
        /// A Taxi Junction is any place where two taxiways meet each other and where they meet either a Parking Spot
        /// or a Runway
        /// </summary>
        [YamlMember(Alias = "junctions")]
        public HashSet<Junction> Junctions { get; set; } = new();

        /// <summary>
        /// A list of Waypoints for inbound and outbound aircraft that are in the air.
        /// </summary>
        [YamlMember(Alias = "waypoints")]
        public HashSet<WayPoint> WayPoints { get; set; } = new();

        public class NavigationPoint
        {
            [YamlMember(Alias = "name")]
            public string Name { get; set; }

            [YamlMember(Alias = "lat")]
            public double Latitude { get; set; }

            [YamlMember(Alias = "lon")]
            public double Longitude { get; set; }

            public override string ToString()
            {
                return $"{Name}: Lat {Latitude} / Lon {Longitude}";
            }
        }

        /// <summary>
        /// An airborne waypoint used for approach and departure handling
        /// </summary>
        public class WayPoint : NavigationPoint {}

        public class TaxiPoint : NavigationPoint {}

        public class ParkingSpot : TaxiPoint {}

        public class Junction : TaxiPoint {}

        public class Runway : TaxiPoint
        {
            [YamlMember(Alias = "heading")]
            public int Heading { get; set; }
        }

        public class NavigationPath
        {
            [YamlMember(Alias = "name")]
            public string Name { get; set; }

            [YamlMember(Alias = "source")]
            public string Source { get; set; }

            [YamlMember(Alias = "target")]
            public string Target { get; set; }

            [YamlMember(Alias = "cost")]
            public int Cost { get; set; }
        }
    }
}
