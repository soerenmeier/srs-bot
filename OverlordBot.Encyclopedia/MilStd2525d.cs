﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using NLog;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace RurouniJones.OverlordBot.Encyclopedia
{
    public class MilStd2525d
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly List<string> _missingCodes = new();

        private static readonly IDeserializer Deserializer = new DeserializerBuilder()
            .WithNamingConvention(UnderscoredNamingConvention.Instance)
            .IgnoreUnmatchedProperties()
            .Build();

        private static readonly HashSet<MilStd2525d> Aircraft = Deserializer.Deserialize<HashSet<MilStd2525d>>(
            File.ReadAllText("Data/Encyclopedia/MilStd2525d/Air.yaml"));
        private static readonly HashSet<MilStd2525d> Vehicles = Deserializer.Deserialize<HashSet<MilStd2525d>>(
            File.ReadAllText("Data/Encyclopedia/MilStd2525d/Land.yaml"));
        private static readonly HashSet<MilStd2525d> Watercraft = Deserializer.Deserialize<HashSet<MilStd2525d>>(
            File.ReadAllText("Data/Encyclopedia/MilStd2525d/Sea.yaml"));

        private static readonly HashSet<MilStd2525d> Units = BuildUnitHashset();

        public string Name { get; set; }
        public string Code { get; set; }
        [YamlMember(Alias = "mil_std_2525_d")]
        public string MilStdCode { get; set; }
        public List<string> DcsCodes { get; set; }

        private static HashSet<MilStd2525d> BuildUnitHashset()
        {
            var set = new HashSet<MilStd2525d>(Aircraft);
            set.UnionWith(Vehicles);
            set.UnionWith(Watercraft);
            return set;
        }

        public static MilStd2525d GetUnitByDcsCode(string code)
        {
            var unit = Units.FirstOrDefault(x => x.DcsCodes.Contains(code));
            if (unit == null)
            {
                if(!_missingCodes.Contains(code))
                {
                    _missingCodes.Add(code);
                    _logger.Warn($"No MIL-STD-2525-D code found for the DCS unit '{code}'. " + 
                        "The https://gitlab.com/overlordbot/srs-bot/-/tree/master/OverlordBot.Encyclopedia/Data/Encyclopedia/MilStd2525d " +
                        "data files need to be updated");
                }
                return null;
            }
            return unit;
        }
    }
}
