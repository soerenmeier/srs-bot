﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NAudio.Wave;
using NLog;
using RurouniJones.OverlordBot.SimpleRadio.Audio;
using RurouniJones.OverlordBot.SimpleRadio.Models;
using RurouniJones.OverlordBot.SimpleRadio.Network.Util;

namespace RurouniJones.OverlordBot.SimpleRadio.Network
{
    internal class AudioClient
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private const int PingDuration = 15;
        private const int PingBuffer = 5;

        private const int TransmissionEndThreshold = 200;

        private readonly ConcurrentQueue<Queue<(byte[], TimeSpan)>> _transmissionQueue;

        private readonly UdpClient _udpClient;

        private readonly ClientInformation _clientInformation;
        private readonly RadioInformation _radioInformation;

        /// <summary>
        /// The timestamp of the last time we received any data from the SRS server.
        /// Used to detect if we have been disconnected
        /// </summary>
        private DateTime _lastReceivedData;

        /// <summary>
        /// The timestamp of the last time we received any audio from the SRS server.
        /// Used to detect if a transmission has finished.
        /// </summary>
        private DateTime _lastReceivedAudio;

        /// <summary>
        /// Do we think there is a transmission currently being made.
        /// </summary>
        private volatile bool _isTransmissionIncoming;

        private ulong _packetNumber = 1;

        private readonly BlockingCollection<byte[]> _encodedAudioPacketByes = new();

        private readonly int _packetNonAudioDataSize = UdpVoicePacket.PacketHeaderLength + UdpVoicePacket.FixedPacketLength + UdpVoicePacket.FrequencySegmentLength;

        public AudioClient(ClientInformation clientInformation, RadioInformation radioInformation, ConcurrentQueue<Queue<(byte[], TimeSpan)>> transmissionQueue)
        {
            _clientInformation = clientInformation;
            _radioInformation = radioInformation;
            _transmissionQueue = transmissionQueue;

            _lastReceivedData = DateTime.Now;

            _udpClient = new UdpClient {Client = {ReceiveTimeout = 5000}};
            if (!OperatingSystem.IsWindows()) return;
            try
            {
                _udpClient.AllowNatTraversal(true);
            }
            catch
            {
                // ignored
            }
        }

        public async Task StartReceivingAudioDataAsync(CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    var localEndpoint = new IPEndPoint(IPAddress.Any, 0);
                    byte[] bytes;

                    try
                    {
                        bytes = _udpClient.Receive(ref localEndpoint);
                    }
                    catch(System.IO.IOException ex)
                    {
                        _logger.Warn(ex);
                        continue;
                    }
                    catch (SocketException)
                    {
                        // There is no API for UDPClient.ReceiveAsync that accepts a cancellation token.
                        // See https://github.com/dotnet/runtime/issues/33418
                        continue;
                    }

                    switch (bytes.Length)
                    {
                        // Something bad here
                        case < 22:
                            continue;
                        //A 22 byte message is a ping response
                        case 22:
                            _lastReceivedData = DateTime.Now;
                            _logger.Trace("Received Audio Ping Response");
                            continue;
                        default:
                            _lastReceivedData = DateTime.Now;
                            try
                            {
                                _encodedAudioPacketByes.Add(bytes, cancellationToken);
                            }
                            catch (OperationCanceledException)
                            {
                                // ignore
                            }

                            break;
                    }
                }
            }, CancellationToken.None);
        }

        public async Task StartDecodingAudioDataAsync(double frequency, RadioInformation.Radio.Modulations modulation, BufferedWaveProvider audioBuffer, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                frequency *= 1000000; // Convert to Hz which is used by SRS networking
                var decoder = new NetworkAudioDecoder(audioBuffer);
                while (!cancellationToken.IsCancellationRequested)
                {
                    Thread.Sleep(50);
                    if (_encodedAudioPacketByes.Count == 0) continue;
                    var encodedOpusAudio = Array.Empty<byte>();
                    try
                    {
                        _encodedAudioPacketByes.TryTake(out encodedOpusAudio, Timeout.Infinite, cancellationToken);
                    }
                    catch (OperationCanceledException)
                    {
                        _logger.Debug("Cancelled operating to get encoded audio");
                    }

                    if (encodedOpusAudio == null || encodedOpusAudio.Length < _packetNonAudioDataSize) continue;

                    _logger.Trace("Audio Data bytes: " + encodedOpusAudio.Length);

                    var udpVoicePacket = UdpVoicePacket.DecodeVoicePacket(encodedOpusAudio);

                    if (!udpVoicePacket.Frequencies.Contains(frequency) || (RadioInformation.Radio.Modulations) udpVoicePacket.Modulations[0] != modulation ) continue;
                    if(!_isTransmissionIncoming) _logger.Debug("Transmission Started");
                    _isTransmissionIncoming = true;
                    _lastReceivedAudio = DateTime.Now;
                    decoder.Decode(udpVoicePacket.AudioPart1Bytes);
                }
            }, CancellationToken.None);
        }

        public async Task StartAudioPingAsync(IPEndPoint endpoint, CancellationToken cancellationToken)
        {
            var message = _clientInformation.Guid;
            await _udpClient.SendAsync(Encoding.ASCII.GetBytes(message), message.Length, endpoint);
            Thread.Sleep(TimeSpan.FromSeconds(2));

            await Task.Run(async () =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    _logger.Trace("Sending Audio Ping");
                    await _udpClient.SendAsync(Encoding.ASCII.GetBytes(message), message.Length, endpoint);
                    Thread.Sleep(TimeSpan.FromSeconds(15));
                }
            }, CancellationToken.None);
        }

        public async Task StartCheckingAudioTimeoutAsync(CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    var elapsedTicks = TimeSpan.FromTicks(DateTime.Now.Ticks - _lastReceivedData.Ticks);
                    _logger.Trace($"Seconds since last Data: {elapsedTicks.Seconds}");
                    if (elapsedTicks.Seconds <= PingDuration * 2 + PingBuffer) continue;
                    _logger.Trace("Audio Client Timeout");
                    break;
                }
            }, CancellationToken.None);
        }

        public async Task StartCheckingForEndOfTransmissions(BufferedWaveProvider audioBuffer, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    Thread.Sleep(TimeSpan.FromMilliseconds(50));
                    if (!_isTransmissionIncoming) continue;
                    var elapsedTicks = TimeSpan.FromTicks(DateTime.Now.Ticks - _lastReceivedAudio.Ticks);
                    _logger.Trace($"Seconds since last Audio Data: {elapsedTicks.Seconds}");
                    if (elapsedTicks.Milliseconds <= TransmissionEndThreshold) continue;
                    // At this point we haven't had any audio data for 200ms so consider the transmission done.
                    // Therefore we write 3 seconds of silence into the AudioBuffer directly so that the speech
                    // recognizer detects the end of the speech and triggers recognition.
                    _isTransmissionIncoming = false;
                     var silence = new byte[audioBuffer.WaveFormat.AverageBytesPerSecond * 3];
                     audioBuffer.AddSamples(silence, 0, silence.Length);
                    _logger.Debug("Transmission Ended");
                }
            }, CancellationToken.None);
        }

        public async Task StartCheckingTransmissionsQueue(IPEndPoint serverEndpoint, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    Thread.Sleep(TimeSpan.FromMilliseconds(50));

                    var isTransmission = _transmissionQueue.TryDequeue(out var transmission);
                    if (!isTransmission) continue;

                    var transmissionThread = new Thread(() => SendTransmission(serverEndpoint, transmission))
                    {
                        Name = "TransmissionSendingThread",
                        Priority = ThreadPriority.Highest
                    };
                    transmissionThread.Start();
                    transmissionThread.Join();

                    // Ensure a 1 second gap between transmissions
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            }, CancellationToken.None);
        }

        public void SendTransmission(IPEndPoint serverEndpoint, Queue<(byte[], TimeSpan)> transmissionPackets)
        {
            var frequencies = new List<double> {_radioInformation.Radios[0].Frequency};
            var encryptions = new List<byte> {0};
            var modulations = new List<byte> {(byte)_radioInformation.Radios[0].Modulation};

            var startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            while (transmissionPackets.Count > 0)
            {
                var (opusPacket, audioTimestamp) = transmissionPackets.Dequeue();
                
                //build SRS packet
                var udpVoicePacket = new UdpVoicePacket
                {
                    GuidBytes = Encoding.ASCII.GetBytes(_clientInformation.Guid),
                    OriginalClientGuidBytes = Encoding.ASCII.GetBytes(_clientInformation.Guid),
                    AudioPart1Bytes = opusPacket,
                    AudioPart1Length = (ushort) opusPacket.Length,
                    Frequencies =  frequencies.ToArray(),
                    UnitId = _radioInformation.UnitId,
                    Encryptions = encryptions.ToArray(),
                    Modulations = modulations.ToArray(),
                    PacketNumber = _packetNumber++
                };

                var encodedUdpVoicePacket = udpVoicePacket.EncodePacket();

                _udpClient.Send(encodedUdpVoicePacket, encodedUdpVoicePacket.Length, serverEndpoint);

                var elapsedTime = DateTimeOffset.Now.ToUnixTimeMilliseconds() - startTime;
                var timeDiff = (int)(elapsedTime - audioTimestamp.TotalMilliseconds);
                var sleepTime = 40 - timeDiff;

                if (sleepTime > 0)
                {
                    Thread.Sleep(sleepTime);
                }
            }
        }
    }
}
